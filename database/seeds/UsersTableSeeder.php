<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'name' => 'administrador',
            'email'=>'administrador@cantina.com',
            'password'=>bcrypt('admin'),
            'sobrenome' => 'admin',
            'telefone'=>'1',
            'cpf'=>'11111111111',
            'rg'=>'1111111',
            'datanascimento'=>'1111-11-11',
            'tipo'=>'admin',
        ]);
    }
}
