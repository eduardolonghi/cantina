<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function sales_produtcs()
    {
        return $this->hasMany('App\SalesProduct', 'id_product');
    }
}
