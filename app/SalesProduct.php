<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesProduct extends Model
{
    public function sale()
    {
        return $this->belongsTo('App\Sale', 'id_sale');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'id_product');
    }
}
