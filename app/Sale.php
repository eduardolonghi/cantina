<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function client()
    {
        return $this->belongsTo('App\Client', 'id_cliente');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public function sales_products()
    {
        return $this->hasMany('App\SalesProduct', 'id_sale');
    }

    public function total_da_venda() {
        $total = 0;

        foreach ($this->sales_products as $sp) {
            $total += $sp->qnt * $sp->product->valor;
        }

        return number_format($total, 2, '.', ',');
    }
}
