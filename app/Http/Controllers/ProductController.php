<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $produtos = Product::all();
        return view ('products/index')->with('produtos', $produtos);
    }

    public function show($id){
        $produto = Product::find($id);
        return view ('products/show')->with('produto',$produto);
    }

    public function create(){
        $produto = new Product();
        return view ('products/create')-> with('produto',$produto);
    }
    public function edit($id){
        $produto = Product::find($id);
        return view ('products/edit')-> with('produto',$produto); //dsadsauoi

    }
    
    public function destroy(Request $request, $id){
        $produto = Product::find($id);
        $produto->delete();

        $request->session()->flash('message', 'Excluido com sucesso');
       return redirect('produtos'); 
    }

    public function store(Request $request){
        $produto = new Product();
        $produto->nome = $request -> input('nome');
        $produto->valor = $request -> input('valor');
        
        if ($produto->save()){
            $request->session()->flash('message','Produto salvo com sucesso.');
        }else{
            $request->session()->flash('message','Ocorreu um erro ao cadastrar.');
        }
        
        return redirect () ->route ('produtos.index');   
        }
   
    public function update(Request $request, $id){
              
   
     
            $produto = Product::find($id);
            $produto->nome = $request->input('nome');
            $produto->valor = $request->input('valor');
            $produto->save();

            $request->session()->flash('message', 'Atualizado com sucesso');

            return redirect('produtos');
          

    }
    
}
