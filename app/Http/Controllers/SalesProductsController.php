<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalesProduct;
use App\Sale;
use App\Product;

class SalesProductsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $vendas_produto = SalesProduct::all();
        return view ('sales_product/index')->with('vendas_produto', $vendas_produto);
    }

    public function show($id){
        $venda_produto = SalesProduct::find($id);
        return view ('sales_product/show')->with('venda_produto',$venda_produto);
    }

    public function create(){
        $venda_produto = new SalesProduct();
        $produtos = Product::all();
        $vendas = Sale::all();
        $idVenda = Sale::all()->last();
        return view ('sales_product/create')-> with(array('venda_produto' => $venda_produto, 'produtos' => $produtos, 'vendas' => $vendas, 'idVenda' => $idVenda));
    }
    public function edit($id){
        $venda_produto = SalesProduct::find($id);
        return view ('sales_product/edit')-> with('venda_produto',$venda_produto);

    }
    
    public function destroy(Request $request, $id){
        $venda_produto = SalesProduct::find($id);
        $venda_produto->delete();

        $request->session()->flash('message', 'Excluido com sucesso');
       return redirect('sales_product'); 
    }

    public function store(Request $request){
        $venda_produto = new SalesProduct();
        $venda_produto->qnt = $request -> input('qnt');
        $venda_produto->id_sale = $request->input('id_sale');
        $venda_produto->id_product = $request -> input('id_product');
        
        if ($venda_produto->save()){
            $request->session()->flash('message','Venda salvo com sucesso.');
        }else{
            $request->session()->flash('message','Ocorreu um erro ao cadastrar.');
        }
        
       return redirect('vendaproduto/create');
        }
   
    public function update(Request $request, $id){

            $venda_produto = SalesProduct::find($id);
            $venda_produto->id_product = $request->input('id_product');
            $venda_produto->id_sale = $request->input('id_sale');
            $venda_produto->qnt = $request->input('qnt');
            $venda_produto->save();

            $request->session()->flash('message', 'Atualizado com sucesso');

            return redirect('vendaproduto/create');

    }
}