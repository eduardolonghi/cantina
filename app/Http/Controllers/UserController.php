<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $usuarios = User::all();
        return view ('users/index')->with('usuarios', $usuarios);
    }

    public function show($id){
        $usuario = User::find($id);
        return view ('users/show')->with('usuario', $usuario);
    }

    public function create(){
        $usuario = new User();
        return view ('users/create')-> with('usuario',$usuario);
    }
    public function edit($id){
        $usuario = User::find($id);
        return view ('users/edit')-> with('usuario',$usuario); //dsadsauoi
    }
    
    public function destroy(Request $request, $id){
        $usuario = User::find($id);
        $usuario->delete();

        $request->session()->flash('message', 'Excluido com sucesso');
       return redirect('usuarios'); 
    }

    public function store(Request $request){
        $usuario = new User();
        $usuario->name = $request -> input('name');
        $usuario->email = $request -> input('email');
        $usuario->password = Hash::make($request ->input('password'));
        $usuario->sobrenome = $request -> input('sobrenome');
        $usuario->telefone = $request -> input('telefone');
        $usuario->cpf = $request -> input('cpf');
        $usuario->rg = $request -> input('rg');
        $usuario->dataNascimento = $request -> input('dataNascimento');
        $usuario->tipo = $request -> input('tipo');
        
        if ($usuario->save()){
            $request->session()->flash('message','Usuário salvo com sucesso.');
        }else{
            $request->session()->flash('message','Ocorreu um erro ao cadastrar.');
        }
        
        return redirect () ->route ('usuarios.index');   
        }
   
    public function update(Request $request, $id){
        $usuario = User::find($id);
        $usuario->name = $request -> input('name');
        $usuario->email = $request -> input('email');
        $usuario->password = $request ->input('password');
        $usuario->sobrenome = $request -> input('sobrenome');
        $usuario->telefone = $request -> input('telefone');
        $usuario->cpf = $request -> input('cpf');
        $usuario->rg = $request -> input('rg');
        $usuario->dataNascimento = $request -> input('dataNascimento');
        $usuario->tipo = $request -> input('tipo');
        $usuario->save();

        $request->session()->flash('message', 'Atualizado com sucesso');

        return redirect('usuarios');
          

    }
    
}
