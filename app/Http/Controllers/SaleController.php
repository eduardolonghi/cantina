<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use App\Client;
use App\User;

class SaleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        $vendas = Sale::all();
        return view ('sales/index')->with('vendas', $vendas);
    }

    public function show($id){
        $venda = Sale::find($id);
        return view ('sales/show')->with('venda',$venda);
    }

    public function create(){
        $venda = new Sale();
        $clientes = Client::all();
        $usuarios = User::all();
        return view ('sales/create')-> with(array('venda' => $venda, 'clientes' => $clientes, 'usuarios' => $usuarios));
    }
    public function edit($id){
        $venda = Sale::find($id);
        return view ('sales/edit')-> with('venda',$venda);

    }
    
    public function destroy(Request $request, $id){
        $venda = Sale::find($id);
        $venda->delete();

        $request->session()->flash('message', 'Excluido com sucesso');
       return redirect('vendas'); 
    }

    public function store(Request $request){
        $venda = new Sale();
        $venda->id_user = $request -> input('id_user');
        $venda->tipopagamento = $request->input('tipopagamento');
        $venda->id_cliente = $request -> input('id_cliente');
        
        if ($venda->save()){
            $request->session()->flash('message','Venda salvo com sucesso.');
        }else{
            $request->session()->flash('message','Ocorreu um erro ao cadastrar.');
        }
        
        return redirect () ->route ('vendaproduto.create');   
        }
   
    public function update(Request $request, $id){

            $venda = Sale::find($id);
            $venda->id_cliente = $request->input('id_cliente');
            $venda->id_user = $request->input('id_user');
            $venda->tipopagamento = $request->input('tipopagamento');
            $venda->save();

            $request->session()->flash('message', 'Atualizado com sucesso');

            return redirect('vendas');

    }
}
