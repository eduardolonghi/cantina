<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $clientes = Client::all();
        return view ('client/index')->with('clientes', $clientes);
    }

    public function show($id){
        $cliente = Client::find($id);
        return view ('client/show')->with('cliente', $cliente);
    }

    public function create(){
        $cliente = new Client();
        return view ('client/create')-> with('cliente',$cliente);
    }
    public function edit($id){
        $cliente = Client::find($id);
        return view ('client/edit')-> with('cliente',$cliente);
    }
    
    public function destroy(Request $request, $id){
        $cliente = Client::find($id);
        $cliente->delete();

        $request->session()->flash('message', 'Excluido com sucesso');
       return redirect('clientes'); 
    }

    public function store(Request $request){
        $cliente = new Client();
        $cliente->nome = $request -> input('nome');
        $cliente->email = $request -> input('email');
        $cliente->sobrenome = $request -> input('sobrenome');
        $cliente->telefone = $request -> input('telefone');
        $cliente->cpf = $request -> input('cpf');
        $cliente->rg = $request -> input('rg');
        $cliente->dataNascimento = $request -> input('dataNascimento');
        
        if ($cliente->save()){
            $request->session()->flash('message','Cliente salvo com sucesso.');
        }else{
            $request->session()->flash('message','Ocorreu um erro ao cadastrar.');
        }
        
        return redirect () ->route ('clientes.index');   
        }
   
    public function update(Request $request, $id){
              
        $cliente = Client::find($id);
        $cliente->nome = $request -> input('nome');
        $cliente->email = $request -> input('email');
        $cliente->sobrenome = $request -> input('sobrenome');
        $cliente->telefone = $request -> input('telefone');
        $cliente->cpf = $request -> input('cpf');
        $cliente->rg = $request -> input('rg');
        $cliente->dataNascimento = $request -> input('dataNascimento');
        $cliente->save();

        $request->session()->flash('message', 'Atualizado com sucesso');

        return redirect('clientes');
          

    }
    
}
