<html>
  <head>
    <meta charset = "utf-8" />
  </head>
  <body>

<?php
  
  $nome = "Eduardo Longhi";

  echo 'Hello '. $nome . '<br />';
  echo "Hello $nome <br />";

  $numero1 = 10;
  $numero2 = 20; 

  echo 'Resultado:  ' . ($numero1 + $numero2) . '<br />';
  echo $numero1 + $numero2 . '<br />';
  echo $numero1 - $numero2 . '<br />';
  echo $numero1 * $numero2 . '<br />';
  echo $numero1 / $numero2 . '<br />';

  // vetor
  $lista = [];
  $lista[] = 'João';
  $lista[] = 'Maria';
  $lista[] = 'Zé';

  // for (para)
  for($i = 0; $i < count($lista); $i++){
    echo "O nome é: $lista[$i] <br />"; 
  }

  foreach($lista as $item){
    echo "O nome é :$item <br />";  
  }
  
  $teste = true;
  
  if ($teste == true) {
    echo 'É verdadeiro <br />';
  } else {
    echo 'É falso <br />';  
  }

  /*
  == - igual
  != - diferente
  || - ou
  && - e
  or - ou com baixa precedência
  and - e com baixa precedência
  === - igual (é verificado valor e tipagem)

  */

  echo 'A hora é: ' . date ("d/m/Y h:I:s"); 

  ?>
  </body>
</html>
