@extends('layouts.app')

@section('content')
  <h2>Edição de Vendas</h2>
  <hr />

{{ Form::model($venda, array('route' => array('vendas.update', $venda->id), 'method' => 'PUT'))}}

  {{Form::label('user','Vendedor:')}}
  {{Form::text('id_user', null, ['class' => 'form-control', 'readonly' => 'true'])}}

  {{Form::label('id_cliente','Cliente:')}}
  {{Form::text('id_cliente', null, ['class' => 'form-control', 'readonly' => 'true'])}}

  {{Form::label('tipoPagamento','Pagamento:')}}
  <select class="form-control" name="tipopagamento">
      <option value="Á vista">Á vista</option>
      <option value="Á prazo">Á prazo</option>
    </select>

  {{Form::submit('Editar',array('class'=> 'btn btn-success'))}}

{{ Form::close() }}

@endsection