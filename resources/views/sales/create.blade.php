@extends('layouts.app')

@section('content')


  <h2>Cadastro de Vendas</h2>
  <hr />

{{ Form::model($venda, array('route' => array('vendas.store')))}}
<!--
  {{Form::label('id_cliente','Cliente:')}}
  {{Form::text('id_cliente')}}

  {{Form::label('id_user','Usuário:')}}
  {{Form::text('id_user')}}
-->

{{ Form::Label('id_cliente', 'Cliente:') }}
  <select class="form-control" name="id_cliente">
    @foreach ($clientes as $cliente)
      <option value="{{$cliente->id}}">{{$cliente->nome}}</option>
    @endforeach
  </select>

{{ Form::Label('id_user', 'Usuário:') }}

  <select class="form-control" name="id_user" >
      <option value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
  </select>

  {{Form::label('tipopagamento','Tipo de Venda:')}}
  <select class="form-control" name="tipopagamento">
      <option value="Á vista">Á vista</option>
      <option value="Á prazo">Á prazo</option>
    </select>

  {{Form::submit('Salvar',array('class'=> 'btn btn-success'))}}

{{ Form::close() }}




  


@endsection