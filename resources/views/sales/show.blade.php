@extends('layouts.app')

@section('content')
<h1>Detalhes da Venda</h1>
<dl class="dl-horizontal">
  <dt>ID</dt>
  <dd>{{ $venda->id }}</dd>
  <dt>Vendedor</dt>
  <dd>{{$venda->user->name}}</dd>
  <dt>Cliente</dt>
  <dd>{{$venda->client->nome}}</dd>
  <dt>Criado em: </dt>
  <dd>{{ $venda->created_at->format('d/m/Y h:i')}}</dd>
  <dt>Atualizado em: </dt>
  <dd>{{ $venda->updated_at->format('d/m/Y h:i') }}</dd>
</dl>
 <a href="/vendas" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>
@endsection