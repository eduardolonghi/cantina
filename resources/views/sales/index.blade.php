@extends('layouts.app')

@section('content')
<div class="row">
<h1>Vendas</h1>

<hr/>
 <a href="/home" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>
<a href="/vendas/create" class ="btn btn-success pull-right">
  Nova Venda
</a>



<br/>
<br/>
<br/>
</div>
<div class="row">
@if (Session::has('message'))
<div class="alert alert-success">
<em> {!! session ('message')!!} </em>
</div>
@endif

<table class="table table-bordered">
<tr>
<th>ID</th>
<th>Tipo de Pagamento</th>
<th>Cliente</th>
<th>Usuário</th>
<th>Total</th>
<th>Ações</th>
</tr>
@foreach ($vendas as $venda)
  <tr>
    <td>{{$venda->id}}</td>
    <td>{{$venda->tipopagamento}}</td>
    <td>{{$venda->client->nome}}</td>
    <td>{{$venda->user->name}}</td>
    <td>{{ $venda->total_da_venda() }}</td>
    <td>
      <a href="/vendas/{{ $venda->id }}" class="pull-left btn btn-default" aria-label="Mostrar Venda">
        <span class= "glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
      </a>

      <a href="/vendas/{{ $venda->id}}/edit" class ="pull-left btn btn-default" style="margin:0px 10px" aria-label="Editar Venda">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
      </a>

      {{ Form::open(array('url' => 'vendas/' . $venda->id, 'class' => 'pull-left')) }}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', array('type' => 'submit', 'class' => 'btn btn-danger')) }}
      {{ Form::close() }}

       </td>

  </tr>
@endforeach
</table>
</div>
@endsection
      


   