@extends('layouts.app')

@section('content')
<h1>Detalhes do Produtos</h1>
<dl class="dl-horizontal">
  <dt>Nome</dt>
  <dd>{{ $produto->nome }}</dd>
  <dt>Valor</dt>
  <dd>{{ $produto->valor }}</dd>
  <dt>Criado em: </dt>
  <dd>{{ $produto->created_at->format('d/m/Y h:i')}}</dd>
  <dt>Atualizado em: </dt>
  <dd>{{ $produto->updated_at->format('d/m/Y h:i') }}</dd>
</dl>
 <a href="/produtos" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>
@endsection