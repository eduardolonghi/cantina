@extends('layouts.app')

@section('content')
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script> 
</head>

  <h2>Cadastro de Produtos</h2>
  <hr />

{{ Form::model($produto, array('route' => array('produtos.store')))}}

  {{Form::label('nome','Nome:')}}
  {{Form::text('nome')}}

  {{Form::label('valor','Valor:')}}
  {{Form::text('valor')}}

  {{Form::submit('Salvar',array('class'=> 'btn btn-success'))}}

   <a href="/produtos" class ="btn btn-default pull-right">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>

{{ Form::close() }}

@endsection