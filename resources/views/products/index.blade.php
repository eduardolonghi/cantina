@extends('layouts.app')

@section('content')
<div class="row">
<h1>Produtos</h1>

<hr/>
 <a href="/home" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>
<a href="/produtos/create" class ="btn btn-success pull-right">
  Novo Produto
</a>



<br/>
<br/>
<br/>
</div>
<div class="row">
@if (Session::has('message'))
<div class="alert alert-success">
<em> {!! session ('message')!!} </em>
</div>
@endif

<table class="table table-bordered">
<tr>
<th>ID</th>
<th>Nome</th>
<th>Valor</th>
<th>Ações</th>
</tr>
@foreach ($produtos as $produto)
  <tr>
    <td>{{$produto->id}}</td>
    <td>{{$produto->nome}}</td>
    <td>{{$produto->valor}}</td>
    <td>
      <a href="/produtos/{{ $produto->id }}" class=" btn btn-default pull-left" aria-label="Mostrar Produto">
        <span class= "glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
      </a>

      <a href="/produtos/{{ $produto->id}}/edit" class ="btn btn-default pull-left" style="margin:0px 10px" aria-label="Editar Produto">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
      </a>

      {{ Form::open(array('url' => 'produtos/' . $produto->id, 'class' => 'pull-left')) }}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', array('type' => 'submit', 'class' => 'btn btn-danger')) }}
      {{ Form::close() }}

       </td>

  </tr>
@endforeach
</table>
</div>
@endsection


   