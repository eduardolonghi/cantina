@extends('layouts.app')

@section('content')
<h1>Detalhes da Venda</h1>
<dl class="dl-horizontal">
  <dt>ID_produto</dt>
  <dd>{{ $venda_produto->id_product}}</dd>
  <dt>Quantidade</dt>
  <dd>{{ $venda_produto->qnt }}</dd>
  <dt>ID_venda</dt>
  <dd>{{ $venda_produto->id_sale }}</dd>
</dl>

@endsection