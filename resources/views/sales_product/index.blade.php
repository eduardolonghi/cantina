@extends('layouts.app')

@section('content')
<div class="row">
<h1>Vendas</h1>

<hr/>

<a href="/vendaproduto/create" class ="btn btn-success pull-right">
  Nova Venda
</a>



<br/>
<br/>
<br/>
</div>
<div class="row">
@if (Session::has('message'))
<div class="alert alert-success">
<em> {!! session ('message')!!} </em>
</div>
@endif

<table class="table table-bordered">
<tr>
<th>ID_produto</th>
<th>Quantidade</th>
<th>ID_Sale</th>
</tr>
@foreach ($vendas_produto as $vendaproduto)
  <tr>
    <td>{{$vendaproduto->id_product}}</td>
    <td>{{$vendaproduto->qnt}}</td>
    <td>{{$vendaproduto->id_sale}}</td>
    <td>
      <a href="/vendaproduto/{{ $vendaproduto->id }}" class=" btn btn-default" aria-label="Mostrar Venda">
        <span class= "glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
      </a>

      <a href="/vendaproduto/{{ $vendaproduto->id}}/edit" class ="btn btn-default" aria-label="Editar Venda">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
      </a>

      {{ Form::open(array('url' => 'vendaproduto/' . $vendaproduto->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete a Venda', array('class' => 'btn btn-warning')) }}
      {{ Form::close() }}

       </td>

  </tr>
@endforeach
</table>
</div>
@endsection
      


   