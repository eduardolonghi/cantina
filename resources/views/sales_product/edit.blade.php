@extends('layouts.app')

@section('content')
  <h2>Edição de Produto</h2>
  <hr />

{{ Form::model($venda_produto, array('route' => array('vendaproduto.update', $venda_produto->id), 'method' => 'PUT'))}}

  {{Form::label('id_product','Produto:')}}
  {{Form::text('id_product', null, ['class' => 'form-control', 'readonly' => 'true'])}}

  {{Form::label('id_sale','Venda:')}}
  {{Form::text('id_sale', null, ['class' => 'form-control', 'readonly' => 'true'])}}

  {{Form::label('qnt','Quantidade:')}}
    <select class="form-control" name="qnt">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
    </select>

  {{Form::submit('Editar',array('class'=> 'btn btn-success'))}}

{{ Form::close() }}

@endsection