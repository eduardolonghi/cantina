@extends('layouts.app')

@section('content')
  <h2>Cadastro de Vendas</h2>
  <hr />

{{ Form::model($venda_produto, array('route' => array('vendaproduto.store')))}}


{{ Form::Label('id_product', 'Produto:') }}
  <select class="form-control" name="id_product">
    @foreach ($produtos as $produto)
      <option value="{{$produto->id}}">{{$produto->nome}}</option>
    @endforeach
  </select>

  {{Form::label('qnt','Quantidade:')}}
  <select class="form-control" name="qnt">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
    </select>

  {{ Form::Label('id_sale', 'ID_VENDA:') }}
  <select class="form-control" name="id_sale">
      <option value="{{$idVenda->id}}">{{$idVenda->id}}</option>
    </select>
</br>
  {{Form::submit('Salvar',array('class'=> 'btn btn-success'))}}
<a href="/vendas" class ="btn btn-success">
  Finalizar Venda
</a>

{{ Form::close() }}



@endsection