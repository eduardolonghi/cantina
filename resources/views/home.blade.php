@extends('layouts.app')

@section('content')
<style>
            .mesmo-tamanho {
                    width: 15%;
                    white-space: normal;
                }
        </style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Início</div>
                    <div class="panel-body">Selecione uma opção:
                        <div class="row"> 
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <a href="/clientes" class ="btn btn-success pull-right" style="width: 250px; height: 40px"> Cadastrar Clientes</a>
                        </div>
                        <div class="col-md-4">
                        </div>
                        </div>
                        </br>

                        <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        <a href="/produtos" class ="btn btn-success pull-right" style="width: 250px; height: 40px">Cadastrar Produtos</a>
                        </div>
                        <div class="col-md-4">
                        </div>
                        </div>
                          </br>
                        <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        <a href="/usuarios" class ="btn btn-success pull-right" style="width: 250px; height: 40px">Cadastrar Usuários</a>
                        </div>
                        <div class="col-md-4">
                        </div>
                        </div>
                        </br>
                        <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                        <a href="/vendas" class ="btn btn-success pull-right" style="width: 250px; height: 40px">Cadastrar Vendas</a>
                        </div>
                        <div class="col-md-4">
                        </div>
                        </div>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
