@extends('layouts.app')
@section('content')
<head>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script> 
  <script>
            $(document).ready(function($){
            $('#cpf').mask('999.999.999-99');
              });

            $(document).ready(function($){
            $('#telefone').mask('(99)99999-9999');
              });

            $(document).ready(function($){
            $('#rg').mask('9.999.999');
              }); 
            $(document).ready(function($){
            $('#dataNascimento').mask('99/99/9999');
              }); 
  </script>

</head>


  <h2>Cadastro de Clientes</h2>
  <hr />

{{ Form::model($cliente, array('route' => array('clientes.store')))}}

<div class="row">
<div class="col-md-1">
  {{Form::label('nome','Nome:')}}
  </div>
<div class="col-md-1">

  {{Form::text('nome')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>



<div class="row">
  <div class="col-md-1">
  {{Form::label('sobrenome','Sobrenome:')}}
  </div>
<div class="col-md-1">

  {{Form::text('sobrenome')}}
  </div>
<div class="col-md-10">
</div>
</div>
</br>

<div class="row">
<div class="col-md-1">

  {{Form::label('email','Email:')}}
   </div>
<div class="col-md-1">
  {{Form::text('email')}}
   </div>
<div class="col-md-10">
</div>
  </div>
  </br>
<div class="row">
<div class="col-md-1">
  {{Form::label('telefone','Telefone:')}}
   </div>
<div class="col-md-1">
  {{Form::text('telefone')}}
   </div>
<div class="col-md-10">
</div>
</div>
</br>
<div class="row">
<div class="col-md-1">
  {{Form::label('dataNascimento','Data de Nascimento:')}}
   </div>
<div class="col-md-1">
  {{Form::text('dataNascimento')}}
   </div>
<div class="col-md-10">
</div>
</div>

<div class="row">
<div class="col-md-1"> 
  {{Form::label('cpf','CPF:')}}
   </div>
<div class="col-md-1">
  {{Form::text('cpf')}}
   </div>
<div class="col-md-10">
</div>
</div>
</br>
<div class="row">
<div class="col-md-1">
  {{Form::label('rg','RG:')}}
   </div>
<div class="col-md-1">
  {{Form::text('rg')}}
   </div>
<div class="col-md-10">
</div>
</div>

  <br />
  <br />
  <br />
<div class="row">
  {{Form::submit('Salvar',array('class'=> 'btn btn-success'))}}

  <a href="/clientes" class ="btn btn-default pull-left" style="margin:0px 10px">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
   </a>
</div>   
{{ Form::close() }}

@endsection