@extends('layouts.app')

@section('content')
<h1>Detalhes do Cliente</h1>
<dl class="dl-horizontal">
  <dt>Nome:</dt>
  <dd>{{ $cliente->nome }}</dd>
  <dt>Sobrenome:</dt>
  <dd>{{ $cliente->sobrenome }}</dd>
  <dt>Email:</dt>
  <dd>{{ $cliente->email }}</dd>
  <dt>Telefone:</dt>
  <dd>{{ $cliente->telefone }}</dd>
  <dt>CPF:</dt>
  <dd>{{ $cliente->cpf }}</dd>
  <dt>RG:</dt>
  <dd>{{ $cliente->rg }}</dd>
  <dt>Data de Nascimento:</dt>
  <dd>{{ $cliente->dataNascimento }}</dd>
</dl>
 <a href="/clientes" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>
@endsection