
@extends('layouts.app')

@section('content')
<div class="row" >
<h1>Clientes</h1>

<hr/>
 <a href="/home" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>
<a href="/clientes/create" class ="btn btn-success pull-right">
  Novo Cliente
</a>



<br/>
<br/>
<br/>
</div>
<div class="row">
@if (Session::has('message'))
<div class="alert alert-success">
<em> {!! session ('message')!!} </em>
</div>
@endif

<table class="table table-bordered">
<tr>
<th>ID</th>
<th>Nome</th>
<th>Sobrenome</th>
<th>Email</th>
<th>Telefone</th>
<th>Cpf</th>
<th>Rg</th>
<th>Data Nascimento</th>
<th>Ações</th>
</tr>
@foreach ($clientes as $cliente)
  <tr>
    <td>{{$cliente->id}}</td>
    <td>{{$cliente->nome}}</td>
    <td>{{$cliente->sobrenome}}</td>
    <td>{{$cliente->email}}</td>
    <td>{{$cliente->telefone}}</td>
    <td>{{$cliente->cpf}}</td>
    <td>{{$cliente->rg}}</td>
    <td>{{$cliente->dataNascimento}}</td>
    
    <td>
      <a href="/clientes/{{ $cliente->id }}" class=" btn btn-default pull-left" aria-label="Mostrar Produto">
        <span class= "glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
      </a>

      <a href="/clientes/{{ $cliente->id}}/edit" class ="btn btn-default pull-left" style="margin:0px 10px" aria-label="Editar Produto">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
      </a>

      {{ Form::open(array('url' => 'clientes/' . $cliente->id, 'class' => 'pull-left')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', array('type' => 'submit', 'class' => 'btn btn-danger')) }}
      {{ Form::close() }}
       </td>

  </tr>
@endforeach
</table>
</div>
@endsection
      


   