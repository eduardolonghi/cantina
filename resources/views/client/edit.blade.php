@extends('layouts.app')

@section('content')
  <h2>Edição de Clientes</h2>
  <hr />

{{ Form::model($cliente, array('route' => array('clientes.update', $cliente->id), 'method' => 'PUT'))}}

  {{Form::label('nome','Nome:')}}
  {{Form::text('nome', null, array('class' => 'form-control'))}}

  {{Form::label('sobrenome','Sobrenome:')}}
  {{Form::text('sobrenome', null, array('class' => 'form-control'))}}
  
  {{Form::label('email','Email:')}}
  {{Form::text('email', null, array('class' => 'form-control'))}}

  {{Form::label('telefone','Telefone:')}}
  {{Form::text('telefone', null, array('class' => 'form-control'))}}

  {{Form::label('cpf','Cpf:')}}
  {{Form::text('cpf', null, array('class' => 'form-control'))}}

  {{Form::label('rg','Rg:')}}
  {{Form::text('rg', null, array('class' => 'form-control'))}}

  {{Form::label('dataNascimento','Data de Nascimento:')}}
  {{Form::text('dataNascimento', null, array('class' => 'form-control'))}}
 </br>
 <a href="/clientes" class ="btn btn-default pull-left">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>

  {{Form::submit('Editar',array('class'=> 'btn btn-success'))}}

{{ Form::close() }}

@endsection