@extends('layouts.app')

@section('content')
<head>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script> 
  <script>
            $(document).ready(function($){
            $('#cpf').mask('999.999.999-99');
              });

            $(document).ready(function($){
            $('#telefone').mask('(99)99999-9999');
              });

            $(document).ready(function($){
            $('#rg').mask('9.999.999');
              }); 
            $(document).ready(function($){
            $('#dataNascimento').mask('99/99/9999');
              }); 
  </script>

</head>


  <h2>Cadastro de Usuários</h2>
  <hr />

{{ Form::model($usuario, array('route' => array('usuarios.store')))}}

  <div class="row">
<div class="col-md-1">
  {{Form::label('name','Nome:')}}
   </div>
<div class="col-md-1">
  {{Form::text('name')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>
  
  <div class="row">
<div class="col-md-1">
  {{Form::label('sobrenome','Sobrenome:')}}
   </div>
<div class="col-md-1">
  {{Form::text('sobrenome')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>
  
  <div class="row">
<div class="col-md-1">
  {{Form::label('email','Email:')}}
   </div>
<div class="col-md-1">
  {{Form::text('email')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>

  <div class="row">
<div class="col-md-1">
  {{Form::label('password','Senha:')}}
   </div>
<div class="col-md-1">
  {{Form::password('password')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>

  <div class="row">
<div class="col-md-1">
  {{Form::label('telefone','Telefone:')}}
   </div>
<div class="col-md-1">
  {{Form::text('telefone')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>

  <div class="row">
<div class="col-md-1">
  {{Form::label('cpf','CPF:')}}
   </div>
<div class="col-md-1">
  {{Form::text('cpf')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>

  <div class="row">
<div class="col-md-1">
  {{Form::label('rg','RG:')}}
   </div>
<div class="col-md-1">
  {{Form::text('rg')}}
  </div>
  <div class="col-md-10">
  </div>
</div>
</br>

  <div class="row">
<div class="col-md-1">
  {{Form::label('dataNascimento','Data de Nascimento:')}}
   </div>
<div class="col-md-1">
  {{Form::text('dataNascimento')}}
   </div>
  <div class="col-md-10">
  </div>
</div>
</br>

  <div class="row">
<div class="col-md-1">
  {{Form::label('tipo','Tipo:')}}
   </div>
<div class="col-md-1">
  {{Form::label('tipo','Administrador')}}
   </div>
<div class="col-md-1">
  {{Form::radio('tipo','Admin')}}
   </div>
<div class="col-md-1">
  {{Form::label('tipo','Usuário')}}
   </div>
<div class="col-md-1">
  {{Form::radio('tipo','Usuario')}}
   </div>
<div class="col-md-7">
</div>

  <br />
  <br />
  <br />
 <a href="/usuarios" class ="btn btn-default pull-left" style="margin:0px 10px">
   <span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
 </a>

  {{Form::submit('Salvar',array('class'=> 'btn btn-success'))}}

{{ Form::close() }}

@endsection