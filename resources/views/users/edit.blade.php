@extends('layouts.app')

@section('content')
  <h2>Edição de Usuários</h2>
  <hr />

{{ Form::model($usuario, array('route' => array('usuarios.update', $usuario->id), 'method' => 'PUT'))}}

  {{Form::label('name','Nome:')}}
  {{Form::text('name', null, array('class' => 'form-control'))}}

  {{Form::label('sobrenome','Sobrenome:')}}
  {{Form::text('sobrenome', null, array('class' => 'form-control'))}}
  
  {{Form::label('email','Email:')}}
  {{Form::text('email', null, array('class' => 'form-control'))}}

  {{Form::label('password','Senha:')}}
  {{Form::text('password', null, array('class' => 'form-control','readonly'))}}

  {{Form::label('telefone','Telefone:')}}
  {{Form::text('telefone', null, array('class' => 'form-control'))}}

  {{Form::label('cpf','Cpf:')}}
  {{Form::text('cpf', null, array('class' => 'form-control'))}}

  {{Form::label('rg','Rg:')}}
  {{Form::text('rg', null, array('class' => 'form-control'))}}

  {{Form::label('dataNascimento','Data de Nascimento:')}}
  {{Form::text('dataNascimento', null, array('class' => 'form-control'))}}

  {{Form::label('tipo','Tipo:')}}
  <br />
  {{Form::label('tipo','Administrador')}}
  {{Form::radio('tipo','Admin')}}
  <br />
  {{Form::label('tipo','Cliente')}}
  {{Form::radio('tipo','Cliente')}}
  <br />
  {{Form::label('tipo','Usuário')}}
  {{Form::radio('tipo','Usuario')}}
  <br />
  <br />
  {{Form::submit('Editar',array('class'=> 'btn btn-success'))}}

{{ Form::close() }}

@endsection