@extends('layouts.app')

@section('content')
<h1>Detalhes do Usuário</h1>
<dl class="dl-horizontal">
  <dt>Nome</dt>
  <dd>{{ $usuario->name }}</dd>
  <dt>Sobrenome</dt>
  <dd>{{ $usuario->sobrenome }}</dd>
  <dt>Email</dt>
  <dd>{{ $usuario->email }}</dd>
  <dt>Telefone</dt>
  <dd>{{ $usuario->telefone }}</dd>
  <dt>Cpf</dt>
  <dd>{{ $usuario->cpf }}</dd>
  <dt>Rg</dt>
  <dd>{{ $usuario->rg }}</dd>
  <dt>Data de Nascimento</dt>
  <dd>{{ $usuario->dataNascimento }}</dd>
  <dt>Tipo</dt>
  <dd>{{ $usuario->tipo }}</dd>

  
</dl>

@endsection