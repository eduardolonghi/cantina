#Projeto Cantina

Esse projeto tem por objetivo avaliar o processo de desenvolvimento de um software na disciplina de Engenharia de Software do curso de Ciência da Computação do Instituto Federal Catarinense - Campus Videira.

Para executar esse projeto execute:

```php -S localhost:8000```

## Dependências 

* PHP >= 5.5
* Mysql Server >= 5.5.54

##Passo-a-passo de como executar a aplicação


* Instalar Github, Mysql Workbench; 
* Instalar o composer disponível em https://getcomposer.org/
* Clonar o projeto utilizando o comando git clone;
* Utilizando o terminal executar o comando composer install;  
* Criar o arquivo. env.example copiando o que está escrito no arquivo .env;
* Alterar os campos DB_DATABASE, DB_USERNAME e DB_PASSWORD;
* Utilizando o terminal executar o comando php artisan key: generate; 
* Utilizando o terminal executar o comando php artisan migrate; 
* Utilizando o terminal executar o comando composer dump-autoload para permitir o seed do banco de dados;
* Executar o comando php artisan db:seed para gerar o usuário para acesso ao sistema;
* Email do usuário padrão é administrador@cantina.com a senha está em UsersTableSeeder.php


