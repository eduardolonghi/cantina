<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

Route::resource('produtos', 'ProductController');

Route::resource('usuarios', 'UserController');

Route::resource('clientes', 'ClientController');

Route::resource('vendas', 'SaleController');

Route::resource('vendaproduto', 'SalesProductsController');

Route::resource('home', 'HomeController');


//Auth::routes();

Route::post('/login/authenticate', ['as' => 'rotaLogin', 'uses' =>'Auth\LoginController@authenticate']);

Route::get('/login', ['as' => 'login', 'uses' =>'HomeController@mostraView']);

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();



